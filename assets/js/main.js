$(document).ready(function(){

	$(".dropdown-menu li a").click(function(){
	  $(".btn:first-child").html('<img src="assets/img/ico-flag.png" alt="" class="img-responsive"> <span class="txt">' + $(this).text()+'</span> <span class="caret"></span>');
	});

	// dropdown dropdown-menu
	$(".notif").click(function(){
	   $(".dropdown-menux").toggle();
	   $(".dropdown-menux2").hide();
	});
	$(".cart").click(function(){
	   $(".dropdown-menux2").toggle();
	   $(".dropdown-menux").hide();
	});

	// banner slide
	$('.slidebanner').slick({
		arrows:true,
		dots:false
	});

	$('.input-group').on('click', 'button[data-toggle-password]', function (event) {
        var button = $(event.target);
        var input = $(event.delegateTarget).find('input');
        var type = button.hasClass('active') ? 'password' : 'text';
        
        input.attr('type', type);
        input.focus();

        button.toggleClass('active');
    });

	// slide product
	$('.slide-product').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: false
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

});



